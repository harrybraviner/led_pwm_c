all : LED_PWM_C.hex LED_PWM_simple.hex

LED_PWM_C.hex : LED_PWM_C.c
	sdcc --use-non-free -mpic16 -p18f2420 LED_PWM_C.c

LED_PWM_simple.hex : LED_PWM_simple.c
	sdcc --use-non-free -mpic16 -p18f2420 LED_PWM_simple.c
