#include <pic18f2420.h>

#pragma config OSC=INTIO67

unsigned int ticker;

void main(void) {
	// We're using the internal clock. Set it to a frequency of 8MHz
	// See page 32 of PIC18F2420 reference manual
	OSCCON = 0x73;

	// Set the PWM period
	// See page 146 of PIC18F2420 reference manual
	// PWM period = (PR2 + 1) * 4 * T_OSC * (TMR2 prescale value)
	PR2 = 0xff;

	// Set Timer 2 to be on, with a prescaler value of 1
	T2CON = T2CON & 0xfc;	// prescaler

	// Set the duty cycle to 512 / 1023
	CCP1CON = CCP1CON & 0xcf;	// LSBs
	CCPR1L = 0x80;			// MSBs
	
	// 100%
	//CCPR1L = 0xff;
	// 25%
	//CCPR1L = 0x3f;
	// 10%
	CCPR1L = 0x66;

	// Make RC2 an output
	TRISC = TRISC & 0xfb;

	T2CON = T2CON | 0x04;	// switch on TMR2

	// Set pin 13 (RC2) as PWM
	CCP1CON = CCP1CON | 0x0c;

	// Set pin 12 (RC1) to be high, so we can compare LED brightness
	TRISC &= 0xfd;
	LATC |= 0x02;

	while(1) { // loop forever
		ticker++;
		if(ticker > 500){
			ticker = 0;
			CCPR1L++;	// Only cycle the 8 MSBs
		}
	}
}
